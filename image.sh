#!/bin/sh
supported_machines="qemu t60 x60 x200"
supported_distros="debian-jessie generic"
arch="i386"

ipxe_revision="827dd1b"
coreboot_revision="0da186c3ff"

####################
# Common functions #
####################

die()
{
  echo "$1 Failed"
  exit 1
}

echo_line()
{
  length="$1"
  pattern="$2"

  while [ "${length}" != "0" ]; do
    echo -n "${pattern}"
    length="$(expr ${length} - 1)"
  done
}

echo_msg()
{
  message="$1"
  msg_len="$(expr $(echo ${message} | wc -c) - 1)"

  echo -n "+"; echo_line "${msg_len}" "-"; echo -n "+"; echo;
  echo -n "|"; echo -n "${message}" ; echo -n "|"; echo "";
  echo -n "+"; echo_line "${msg_len}" "-"; echo -n "+"; echo;
}

check_file()
{
    msg_prefix="$1"
    file="$2"
    [ ! -f ${file} ] && die "${msg_prefix} ERROR: ${file} not found"
}

check_dir()
{
    msg_prefix="$1"
    dir="$2"
    [ ! -d ${dir} ] && die "${msg_prefix} ERROR: ${dir} not found"
}

cp_check()
{
    msg_prefix="$1"
    src="$2"
    dst="$3"
    cp "${src}" "${dst}" || die "${msg_prefix} cp ${src} ${dst} failed"
}

check_size()
{
    msg_prefix="$1"
    file_path="$2"
    expected_size="$3"
    calculated_size="$(du -b ${file_path} | awk '{print $1}')"
    if [ "${expected_size}" != "${calculated_size}" ] ; then
      die "${msg_prefix} Wrong file size for ${file_path} ${expected_size} expected, got ${calculated_size}. Size check"
    fi
}

########################################
# Usage and cmdline arguments handling #
########################################

# Produces something like that:
# qemu|t60|x60 with "qemu t60 x60"
usage_print_support()
{
    list="$1"
    list_len="$(echo ${list} | awk '{print NF}')"

    i=0;
    for elm in ${list} ; do
	i=$(expr $i + 1);
	if [ "$i" = "${list_len}" ] ; then
	    printf "%s" "${elm}"
	else
	    printf "%s|" "${elm}"
	fi
    done
}

usage()
{
    # Prints something like that:
    # Usage: ./image.sh <qemu|t60|x60|x200> <debian-jessie|generic>
    printf 'Usage: %s <' "$0"
    usage_print_support "${supported_machines}"
    printf '> <'
    usage_print_support "${supported_distros}"
    printf '>\n'
}

check_usage()
{
    var="$1"
    list="$2"

    found=0
    for elm in ${list} ; do
	[ "${var}" = "${elm}" ] && found=1
    done

    if [ "${found}" != "1" ] ; then
	printf "%s not found in <%s>\n\n" "${var}" \
	       "$(usage_print_support \"${list}\")"
	usage
	exit 1
    fi
}

#################
# git functions #
#################

# We want to make sure that shorter and longer hash matches.
# for instance when 827dd1b to 827dd1bf is the same revision.
# So to do that we ask git to compute again the revision.
get_unique_git_revision()
{
    directory="$1"
    revision="$2"
    echo "$(git -C ${directory} log --oneline ${revision} 2>/dev/null | head -n1 | awk '{print $1}')"
}

update_git()
{
  dir="$1"
  rev="$2"

  check_dir "[update]" "${dir}"

  # Fetch the revision if it's not in git:
  # If git doesn't know have the hash, it returns a blank revision
  # and print the following error message on stderr:
  # "unknown revision or path not in the working tree."
  # So we detect if the hash is present, and if not we fetch it.
  if [ "" == "$(get_unique_git_revision ${dir} ${rev})" ] ; then
      echo_msg "[update] ${dir} to ${rev}"
      git -C "${dir}" fetch origin || die "[update] ${dir}@${rev}"
  else
      echo "[update] ${dir}: revision ${rev} already present, skipping fetch."
  fi

  git -C "${dir}" checkout -f "${rev}"
}

clone_project()
{
    uri="$1"
    project_dir="$2"
    project_name="$(basename ${project_dir})"

    if [ ! -d "${project_dir}" ] ; then
	echo_msg "[clone] ${project_name}"
	git clone "${uri}" \
	    "${project_dir}" || die "[clone] ${project_dir}"
    else
	echo "[clone] ${project_name}: ${project_dir} present, skipping."
    fi
}

apply_git_patch()
{
    msg_prefix="$1"
    dir="$2"
    patch="$3"

    check_file "${msg_prefix}" "${patch}"
    echo git -C "${dir}" apply "${patch}"
    git -C "${dir}" apply "${patch}" || \
	die "${msg_prefix} applying ${patch} failed"
}

apply_multiple_git_patches()
{
    msg_prefix="$1"
    dir="$2"
    patches_dir="$3"

    shift 3
    for patch in $@ ; do
	apply_git_patch "${msg_prefix}" "${dir}" "${patches_dir}/${patch}"
    done
}

###################
# build functions #
###################
apply_coreboot_config()
{
    config="$1"
    cp_check "[build]" "${config}" "${coreboot_dir}/.config"
    yes '' | make -C ${coreboot_dir} oldconfig || die "coreboot config"
}

build_coreboot()
{
    config="$1"
    config_name="$2"
    # Here we want an final file containing two coreboot images:
    # One with grub, and another one with SeaBIOS+iPXE.
    # Coreboot has a mechanism to permit that. It uses different prefixes.
    # The coreboot wiki has more details on it here:
    # https://www.coreboot.org/Fallback_mechanism
    # Here GRUB will be in the "fallback/" prefix, and one in the "normal/"
    # prefix. With our configuration, building an image with the "normal/"
    # prefix requires an image already built with the "fallback/" prefix.
    fallback_image_path="$3"

    echo_msg "[build] coreboot with ${config_name}"
    apply_coreboot_config "${config}"

    make -C ${coreboot_dir} clean || die "coreboot clean"

    if [ -n "${fallback_image_path}" ] ; then
	# With CONFIG_UPDATE_IMAGE=y coreboot expects an existing image in
	# build/coreboot.rom.
	mkdir -p "${coreboot_dir}/build/"
	check_dir "[setup]" "${coreboot_dir}/build/"
	cp_check "[build]" "${fallback_image_path}" \
		 "${coreboot_dir}/build/coreboot.rom"
    fi

    make -C ${coreboot_dir} || die "coreboot"
    check_file "[build]" "${coreboot_dir}/build/coreboot.rom"
}

build_toolchain()
{
    # Make sure that the toolchain is built and produces .xcompile
    echo_msg "[build] Coreboot toolchain"
    make -C ${coreboot_dir} "crossgcc-${arch}" CPUS="${jobs}" || \
	die "[build] ERROR: coreboot toolchain build failed"

    # Necessary to produce the .xcompile.
    apply_coreboot_config \
	"${config_dir}/machines/${machine}/coreboot-ipxe/defconfig"

    make -C ${coreboot_dir} CPUS="${jobs}" .xcompile || \
	die "[build] ERROR: coreboot toolchain: could not generate .xcompile"

    check_file "[build]" "${coreboot_dir}/.xcompile"
}

build_ich9gen()
{
    make -C "${libreboot_dir}/resources/utilities/ich9deblob"
}

if [ $# -ne 2 ] ; then
  usage
  exit 1
fi

machine="$1"
distro="$2"
check_usage "${machine}" "${supported_machines}"
check_usage "${distro}" "${supported_distros}"

top_dir="$(pwd)"
config_dir="${top_dir}/configs"

ipxe_dir="build/ipxe"
# We want an iPXE binary that supports as much boards as possible.
# Not only that simplifies the code, but it also permits users to use iPXE on
# other network cards than the one built-in the target machine.
ipxe_target="ipxe.dsk"
coreboot_dir="build/coreboot"
libreboot_dir="build/libreboot"
grub_dir="${coreboot_dir}/payloads/external/GRUB2/grub2"

#####################
#### Build setup ####
#####################
echo_msg "[setup] checking files and directories"

jobs="$(cat /proc/cpuinfo | grep '^processor' | wc -l)"

check_file "[setup]" "$(pwd)/image.sh"

# Build configuration
check_file "[setup]" "${config_dir}/generic/seabios/defconfig"
check_file "[setup]" "${config_dir}/machines/${machine}/coreboot-ipxe/defconfig"
check_file "[setup]" "${config_dir}/machines/${machine}/coreboot-grub/defconfig"

mkdir -p "build/${machine}"; check_dir "[setup]" "build/${machine}"
mkdir -p "out/${machine}"; check_dir "[setup]" "out/${machine}"

clone_project "git://git.ipxe.org/ipxe.git" "${ipxe_dir}"
clone_project "https://review.coreboot.org/p/coreboot" "${coreboot_dir}"
clone_project "https://notabug.org/vimuser/libreboot.git" "${libreboot_dir}"
update_git "${ipxe_dir}" "${ipxe_revision}"
update_git "${coreboot_dir}" "${coreboot_revision}"

check_dir "[setup]" "${ipxe_dir}/src"

###############
# Build phase #
###############

# Apply coreboot patches.
apply_multiple_git_patches \
    "[build]" "${coreboot_dir}" \
    "${config_dir}/generic/patches/coreboot/" \
    0001-payloads-external-GRUB2-Fix-build-error.patch \
    0001-genbuild_h-git-builds-Make-date-1-lang-locale-and-ti.patch \
    0001-NOT-FOR-MERGE-payloads-external-GRUB2-make-it-reprod.patch \
    #

# Make sure that ${grub_dir} is created
make -C "${coreboot_dir}/payloads/external/GRUB2"

# Then apply GRUB patches

# As GRUB sources are part of coreboot, they are not handled by this script.
# Becuase of that, they are not checked out with git checkout -f, so we need to
# ensure that the repository is clean to apply them
git -C "${grub_dir}" reset --hard
apply_multiple_git_patches \
    "[build]" \
    "${grub_dir}" \
    "${config_dir}/generic/patches/grub/" \
    0001-mkstandalone-add-argument-fixed-time-to-override-mti.patch \
    0002-mkrescue-add-argument-fixed-time-to-get-reproducible.patch \
    0003-Makefile-use-FIXED_TIMESTAMP-for-mkstandalone-if-set.patch \
    0001-modinfo.sh.in-Remove-build-specific-information-for-.patch \
    #

# We use coreboot toolchain, so we need to build it first.
build_toolchain

# Apply iPXE patches
apply_multiple_git_patches \
    "[build]" \
    "${ipxe_dir}" \
    "${config_dir}/generic/patches/ipxe/" \
    0001-digest-Add-sha256sum-command.patch \
    0001-build-Make-it-deterministic-when-building-from-git.patch \
    #

# Compile iPXE with coreboot toolchain
CROSS_COMPILE="$(grep "^# TARCH_SEARCH=" "${coreboot_dir}/.xcompile" | grep ${arch} | awk '{print $3}')"


IPXE_OPTIONS="bin/${ipxe_target} NO_WERROR=1 CROSS_COMPILE=${CROSS_COMPILE} EMBED=${config_dir}/generic/distros/${distro}/install.ipxe"

# Add iPXE Settings
cp_check "[build]" \
	 "${config_dir}/generic/ipxe/general.h" "${ipxe_dir}/src/config/local/"

# Finally build ipxe
echo_msg "[build] iPXE"
make -C "${ipxe_dir}/src" clean || die "ipxe clean"
make -j${jobs} -C "${ipxe_dir}/src" ${IPXE_OPTIONS} || die "ipxe"
check_file "[build]" "${ipxe_dir}/src/bin/${ipxe_target}"

# We are using a floppy disk image, however SeaBIOS seem to only use files that
# matches the size of real floppy disk (when uncompressed). So we make it match
# by padding it.
floppy_size="$(expr 720 \* 1024)"
ipxe_size="$(du -b ${ipxe_dir}/src/bin/${ipxe_target} | awk '{print $1}')"
blank_file="$(mktemp /tmp/blank_file.XXX)"
output_file="$(mktemp /tmp/output_file.XXX)"
ddrescue -s "$(expr ${floppy_size} - ${ipxe_size})" /dev/zero "${blank_file}"
cat "${ipxe_dir}/src/bin/${ipxe_target}" "${blank_file}" > "${output_file}"
mv -f "${output_file}" "${ipxe_dir}/src/bin/${ipxe_target}"

check_size "[build]" "${ipxe_dir}/src/bin/${ipxe_target}" "${floppy_size}"

# Setup coreboot builds
ln -sf "$(pwd)/${ipxe_dir}/src/bin/${ipxe_target}" \
   "${coreboot_dir}/${ipxe_target}"
cp_check "[build]" "${config_dir}/generic/seabios/defconfig" \
	 "${coreboot_dir}/seabios-defconfig"

# We compile "fallback/" first
build_coreboot \
	       "${config_dir}/machines/${machine}/coreboot-grub/defconfig" \
	       "GRUB" \
	       ""
cp_check "[build]" "${coreboot_dir}/build/coreboot.rom" \
	 "build/${machine}/coreboot_${distro}_grub.rom"

# Then "normal/"
build_coreboot "${config_dir}/machines/${machine}/coreboot-ipxe/defconfig" \
	       "iPXE" "build/${machine}/coreboot_${distro}_grub.rom"
make -C "${coreboot_dir}/util/cbfstool/"

"${coreboot_dir}/util/cbfstool/cbfstool" \
				 "${coreboot_dir}/build/coreboot.rom" \
				 add -f "${ipxe_dir}/src/bin/${ipxe_target}" \
				-n "floppyimg/${ipxe_target}" -t raw

if [ "${machine}" = "x200" ] ; then
    build_ich9gen
    "${libreboot_dir}/resources/utilities/ich9deblob/ich9gen" \
	--macaddress 00:1f:16:00:00:00 ;
    mv ich9fdgbe_16m.bin ich9fdgbe_8m.bin ich9fdnogbe_4m.bin ich9fdgbe_4m.bin \
       ich9fdnogbe_16m.bin ich9fdnogbe_8m.bin mkgbe.c mkgbe.h build/
    dd if=build/ich9fdgbe_8m.bin "of=${coreboot_dir}/build/coreboot.rom" bs=1 count=12k conv=notrunc
    echo "Warning: The resulting image has a generic MAC address for your Ethernet card"
fi

cp_check "[build]" "${coreboot_dir}/build/coreboot.rom" \
	 "out/${machine}/coreboot_${distro}_combined.rom"

################
# Final checks #
################
echo_msg "[checksums]"
sha1sum "out/${machine}/coreboot_${distro}_combined.rom"
