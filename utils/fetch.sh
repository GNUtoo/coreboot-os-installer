#!/bin/sh
top_dir="$(dirname $0)/.."
coreboot_dir="${top_dir}/build/coreboot"
libreboot_dir="build/libreboot"
ipxe_dir="${top_dir}/build/ipxe"
payloads_dir="${coreboot_dir}/payloads/external/"

git_fetch()
{
    repo="$1"
    dir="$2"

    if [ ! -d "${dir}" ] ; then
	git clone "${repo}" "${dir}"
    fi
}

# Fetch standalone projects
git_fetch "git://git.ipxe.org/ipxe.git" "${ipxe_dir}"
git_fetch "https://review.coreboot.org/p/coreboot" "${coreboot_dir}"
git_fetch "https://notabug.org/vimuser/libreboot.git" "${libreboot_dir}"

# Fetch the coreboot payloads
make -C "${payloads_dir}/SeaBIOS/"
make -C "${payloads_dir}/GRUB2/"
