#!/bin/sh
arch="x86"

diff_command="diff -u"
meld_command="meld"
checksum_command="sha1sum"

coreboot_dir="$(dirname $0)/../build/coreboot"
cbfstool="${coreboot_dir}/util/cbfstool/cbfstool"

build_cbfstool()
{
    if [ ! -d "${coreboot_dir}" ] ; then
	echo "No ${coreboot_dir} directory:"
	echo "You need to build an image with image.sh to have this directory created for you"
	exit 1
    elif [ ! -f "${cbfstool}" ] ; then
	make -C "$(dirname ${cbfstool})"
    fi
}

# This extracts each cbfs file in a directory
cbfs_extract()
{
    image="$1"
    directory="$2"

    if [ ! -d "${directory}" ] ; then
	echo "Error: ${directory} is not directory, does not exist or is not accesible:"
	echo "You can create it with the following command:"
	echo "mkdir -p ${directory}"
	exit 1
    fi

    "${cbfstool}" "${image}" print 2>/dev/null | \
	# Print the cbfs path field and skip the first line(fields description)
	awk 'BEGIN{found=0} {if(line > 1){print $1} else{line++}}' | \
	while read cbfs_name ; do
	    [ "${cbfs_name}" = "(empty)" ] && continue
	    # We need to handle files with the same cbfs name in a different
	    # directory such as normal/payload and fallback/payload
	    file_name="$(echo ${cbfs_name} | sed 's#/#_#g')"
	    "${cbfstool}" "${image}" extract \
			  -n "${cbfs_name}" \
			  -f "${directory}/${file_name}" \
			  -m "${arch}" 1>/dev/null 2>/dev/null
	done | sort
}

# This print the checksums of each cbfs file
# The output is sorted by hash for easy comparison
cbfs_checksum()
{
    image="$1"
    tmpfile="$(mktemp -u)"
    "${cbfstool}" "${image}" print 2>/dev/null | \
	# Print the cbfs path field and skip the first line(fields description)
	awk 'BEGIN{found=0} {if(line > 1){print $1} else{line++}}' | \
	while read name ; do
	    [ "${name}" = "(empty)" ] && continue
	    "${cbfstool}" "${image}" extract -n "${name}" -f "${tmpfile}" -m "${arch}" 1>/dev/null 2>/dev/null
	    printf "%s  %s\n" "$(${checksum_command} ${tmpfile} | awk '{print $1}')" "${name}"
	done | sort
}

usage()
{
    echo "$0 <path/to/coreboot.rom>"
    echo "$0 diff <path/to/coreboot.rom> <path/to/other/coreboot.rom>"
    echo "$0 meld <path/to/coreboot.rom> <path/to/other/coreboot.rom>"
    echo "$0 extract <path/to/coreboot.rom> [path/to/directory]"

    return 1
}

if [ $# -eq 1 ] ; then
    build_cbfstool
    cbfs_checksum "$1"
elif [ $# -eq 3 ] && [ "$1" = "diff" -o "$1" = "meld" ] ; then
    first_checksums="$(mktemp -u)"
    second_checksums="$(mktemp -u)"
    comparison_command=""

    if [ "$1" = "diff" ] ; then
	comparison_command="${diff_command}"
    elif [ "$1" = "meld" ] ; then
	comparison_command="${meld_command}"
    else
        usage
    fi
    build_cbfstool
    cbfs_checksum "$2" > "${first_checksums}"
    cbfs_checksum "$3" > "${second_checksums}"
    ${comparison_command} "${first_checksums}" "${second_checksums}"
elif [ $# -eq 3 ] && [ "$1" = "extract" ] ; then
    cbfs_extract "$2" "$3"
else
    usage
fi
