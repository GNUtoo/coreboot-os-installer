#!/bin/sh
arch="i386"

if [ $# -lt 1 ] ; then
    echo "$0 <image_path> [qemu options]"
    exit 1
fi

coreboot_image_path="$1"
disk_image_path="$(dirname $0)/../out/qemu/qemu.img"

shift 1

mkdir -p "$(dirname ${disk_image_path})"
[ -f "${disk_image_path}" ] || qemu-img create "${disk_image_path}" 4G

"qemu-system-${arch}" \
-m 256M -M pc \
-hda "${disk_image_path}" \
-serial stdio -enable-kvm \
-bios "${coreboot_image_path}" \
$@
