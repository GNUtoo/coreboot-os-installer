.PHONY: archive all

all:
	@echo "Available commands:"
	@echo "  make archive"
	@echo "  To create images, run image.sh instead"

archive:
	git archive --format=tar -o build/recovery.tar --prefix=recovery/ HEAD
	mkdir -p build
	rm -f build/recovery.tar.xz
	xz -9 build/recovery.tar
	# /!\ Warning, dangreous as it might include git hooks
	# Find a better way to do it
	tar cJf build/recovery.git.tar.xz .git --transform='s/^/recovery\//'
